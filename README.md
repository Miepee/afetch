# afetch

another fetch clone written in C++

## Dependencies

- gcc
- make

## Build instructions

`$ make`		to compile the application <br>
`# make install`	to install the compiled binary <br>
`# make uninstall`	to uninstall the application <br>
